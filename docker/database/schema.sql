-- Adminer 4.8.0 MySQL 8.0.23 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `abnormal_claim`
(
    `id`               bigint unsigned                                              NOT NULL AUTO_INCREMENT,
    `name`             varchar(1024) COLLATE utf8mb4_unicode_ci                     NOT NULL,
    `claim_id`         varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    `is_update`        tinyint(1)                                                   NOT NULL DEFAULT '0',
    `block_hash`       varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci            DEFAULT NULL,
    `transaction_hash` varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci            DEFAULT NULL,
    `vout`             int unsigned                                                 NOT NULL,
    `output_id`        bigint unsigned                                              NOT NULL,
    `value_as_hex`     mediumtext COLLATE utf8mb4_unicode_ci                        NOT NULL,
    `value_as_json`    mediumtext COLLATE utf8mb4_unicode_ci,
    `created_at`       datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`      datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `Idx_UnknowClaimBlockHash` (`block_hash`),
    KEY `Idx_UnknowClaimOutput` (`output_id`),
    KEY `Idx_UnknowClaimTxHash` (`transaction_hash`),
    CONSTRAINT `abnormal_claim_ibfk_1` FOREIGN KEY (`output_id`) REFERENCES `output` (`id`) ON DELETE CASCADE,
    CONSTRAINT `Cnt_ValueValidJson` CHECK (((`value_as_json` is null) or json_valid(`value_as_json`)))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `address`
(
    `id`          bigint unsigned                                            NOT NULL AUTO_INCREMENT,
    `address`     varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `first_seen`  datetime                                                            DEFAULT NULL,
    `created_at`  datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at` datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `balance`     double(58, 8)                                              NOT NULL DEFAULT '0.00000000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `address` (`address`),
    UNIQUE KEY `Idx_AddressAddress` (`address`),
    UNIQUE KEY `address_2` (`address`),
    KEY `Idx_AddressCreated` (`created_at`),
    KEY `Idx_AddressModified` (`modified_at`),
    KEY `balance` (`balance`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `application_status`
(
    `id`           bigint unsigned NOT NULL AUTO_INCREMENT,
    `app_version`  int             NOT NULL,
    `data_version` int             NOT NULL,
    `api_version`  int             NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `block`
(
    `id`                     bigint unsigned                                            NOT NULL AUTO_INCREMENT,
    `bits`                   varchar(20) COLLATE utf8mb4_unicode_ci                     NOT NULL,
    `chainwork`              varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `confirmations`          int unsigned                                               NOT NULL,
    `difficulty`             double(50, 8)                                              NOT NULL,
    `hash`                   varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `height`                 bigint unsigned                                            NOT NULL,
    `merkle_root`            varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `name_claim_root`        varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `nonce`                  bigint unsigned                                            NOT NULL,
    `previous_block_hash`    varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci          DEFAULT NULL,
    `next_block_hash`        varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci          DEFAULT NULL,
    `block_size`             bigint unsigned                                            NOT NULL,
    `block_time`             bigint unsigned                                            NOT NULL,
    `version`                bigint unsigned                                            NOT NULL,
    `version_hex`            varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `transaction_hashes`     mediumtext COLLATE utf8mb4_unicode_ci,
    `transactions_processed` tinyint(1)                                                 NOT NULL DEFAULT '0',
    `created_at`             datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`            datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `Idx_BlockHash` (`hash`),
    KEY `Idx_BlockHeight` (`height`),
    KEY `Idx_BlockTime` (`block_time`),
    KEY `Idx_PreviousBlockHash` (`previous_block_hash`),
    KEY `Idx_BlockCreated` (`created_at`),
    KEY `Idx_BlockModified` (`modified_at`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `claim`
(
    `id`                      bigint unsigned                                              NOT NULL AUTO_INCREMENT,
    `transaction_hash_id`     varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci            DEFAULT NULL,
    `vout`                    int unsigned                                                 NOT NULL,
    `name`                    varchar(1024) COLLATE utf8mb4_unicode_ci                     NOT NULL,
    `claim_id`                varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    `claim_type`              tinyint                                                      NOT NULL,
    `publisher_id`            char(40) CHARACTER SET latin1 COLLATE latin1_general_ci               DEFAULT NULL COMMENT 'references a ClaimId with CertificateType',
    `publisher_sig`           varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci           DEFAULT NULL,
    `certificate`             text COLLATE utf8mb4_unicode_ci,
    `sd_hash`                 varchar(120) CHARACTER SET latin1 COLLATE latin1_general_ci           DEFAULT NULL,
    `transaction_time`        bigint unsigned                                                       DEFAULT NULL,
    `version`                 varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci   NOT NULL,
    `value_as_hex`            mediumtext COLLATE utf8mb4_unicode_ci                        NOT NULL,
    `value_as_json`           mediumtext COLLATE utf8mb4_unicode_ci,
    `valid_at_height`         int unsigned                                                 NOT NULL,
    `height`                  int unsigned                                                 NOT NULL,
    `effective_amount`        bigint unsigned                                              NOT NULL DEFAULT '0',
    `author`                  varchar(512) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `description`             mediumtext COLLATE utf8mb4_unicode_ci,
    `content_type`            varchar(162) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         DEFAULT NULL,
    `is_nsfw`                 tinyint(1)                                                   NOT NULL DEFAULT '0',
    `language`                varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci            DEFAULT NULL,
    `thumbnail_url`           text COLLATE utf8mb4_unicode_ci,
    `title`                   text COLLATE utf8mb4_unicode_ci,
    `fee`                     double(58, 8)                                                NOT NULL DEFAULT '0.00000000',
    `fee_currency`            char(30) COLLATE utf8mb4_unicode_ci                                   DEFAULT NULL,
    `fee_address`             varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci   NOT NULL,
    `is_filtered`             tinyint(1)                                                   NOT NULL DEFAULT '0',
    `bid_state`               varchar(20) COLLATE utf8mb4_unicode_ci                       NOT NULL DEFAULT 'Accepted',
    `created_at`              datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`             datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `claim_address`           varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci   NOT NULL DEFAULT '',
    `is_cert_valid`           tinyint(1)                                                   NOT NULL,
    `is_cert_processed`       tinyint(1)                                                   NOT NULL,
    `license`                 text COLLATE utf8mb4_unicode_ci,
    `license_url`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci               DEFAULT NULL,
    `preview`                 varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci               DEFAULT NULL,
    `type`                    varchar(100) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `release_time`            bigint unsigned                                                       DEFAULT NULL,
    `source_hash`             varchar(255) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `source_name`             varchar(255) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `source_size`             bigint unsigned                                                       DEFAULT NULL,
    `source_media_type`       varchar(255) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `source_url`              varchar(255) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `frame_width`             bigint unsigned                                                       DEFAULT NULL,
    `frame_height`            bigint unsigned                                                       DEFAULT NULL,
    `duration`                bigint unsigned                                                       DEFAULT NULL,
    `audio_duration`          bigint unsigned                                                       DEFAULT NULL,
    `os`                      varchar(100) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `email`                   varchar(255) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `website_url`             varchar(255) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `has_claim_list`          tinyint(1)                                                            DEFAULT NULL,
    `claim_reference`         varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT NULL,
    `list_type`               smallint                                                              DEFAULT NULL,
    `claim_id_list`           json                                                                  DEFAULT NULL,
    `country`                 varchar(100) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `state`                   varchar(100) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `city`                    varchar(100) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `code`                    varchar(100) COLLATE utf8mb4_unicode_ci                               DEFAULT NULL,
    `latitude`                bigint                                                                DEFAULT NULL,
    `longitude`               bigint                                                                DEFAULT NULL,
    `transaction_hash_update` varchar(70) COLLATE utf8mb4_unicode_ci                                DEFAULT NULL,
    `vout_update`             int unsigned                                                          DEFAULT NULL,
    `claim_count`             bigint                                                       NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `Idx_ClaimUnique` (`transaction_hash_id`, `vout`, `claim_id`),
    KEY `FK_ClaimPublisher` (`publisher_id`),
    KEY `Idx_Claim` (`claim_id`),
    KEY `Idx_ClaimTransactionTime` (`transaction_time`),
    KEY `Idx_ClaimCreated` (`created_at`),
    KEY `Idx_ClaimModified` (`modified_at`),
    KEY `Idx_ClaimValidAtHeight` (`valid_at_height`),
    KEY `Idx_ClaimBidState` (`bid_state`),
    KEY `Idx_ClaimName` (`name`(255)),
    KEY `Idx_ClaimAuthor` (`author`(191)),
    KEY `Idx_ClaimContentType` (`content_type`),
    KEY `Idx_ClaimLanguage` (`language`),
    KEY `Idx_ClaimTitle` (`title`(191)),
    KEY `Idx_FeeAddress` (`fee_address`),
    KEY `Idx_ClaimOutpoint` (`transaction_hash_id`, `vout`) COMMENT 'used for match claim to output with joins',
    KEY `Idx_ClaimAddress` (`claim_address`),
    KEY `Idx_Height` (`height`),
    KEY `idx_cert_valid` (`is_cert_valid`),
    KEY `idx_cert_processed` (`is_cert_processed`),
    KEY `Idx_LicenseURL` (`license_url`),
    KEY `Idx_Preview` (`preview`),
    KEY `Idx_type` (`type`),
    KEY `Idx_release_time` (`release_time`),
    KEY `Idx_source_hash` (`source_hash`),
    KEY `Idx_source_name` (`source_name`),
    KEY `Idx_source_size` (`source_size`),
    KEY `Idx_source_media_type` (`source_media_type`),
    KEY `Idx_source_url` (`source_url`),
    KEY `Idx_frame_width` (`frame_width`),
    KEY `Idx_frame_height` (`frame_height`),
    KEY `Idx_duration` (`duration`),
    KEY `Idx_audio_duration` (`audio_duration`),
    KEY `Idx_os` (`os`),
    KEY `Idx_email` (`email`),
    KEY `Idx_website_url` (`website_url`),
    KEY `Idx_has_claim_list` (`has_claim_list`),
    KEY `Idx_claim_reference` (`claim_reference`),
    KEY `Idx_list_type` (`list_type`),
    KEY `Idx_country` (`country`),
    KEY `Idx_state` (`state`),
    KEY `Idx_city` (`city`),
    KEY `Idx_latitude` (`latitude`),
    KEY `Idx_longitude` (`longitude`),
    KEY `transaction_hash_update` (`transaction_hash_update`, `vout_update`),
    KEY `sd_hash` (`sd_hash`),
    KEY `claim_count` (`claim_count`),
    CONSTRAINT `claim_ibfk_1` FOREIGN KEY (`transaction_hash_id`) REFERENCES `transaction` (`hash`) ON DELETE CASCADE,
    CONSTRAINT `Cnt_ClaimCertificate` CHECK (((`certificate` is null) or json_valid(`certificate`)))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `claim_in_list`
(
    `id`            bigint unsigned                                              NOT NULL AUTO_INCREMENT,
    `list_claim_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    `claim_id`      varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci          DEFAULT NULL,
    `created_at`    datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`   datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `Idx_claim_tag` (`list_claim_id`, `claim_id`),
    KEY `Idx_OuptutCreated` (`created_at`),
    KEY `Idx_OutputModified` (`modified_at`),
    CONSTRAINT `claim_in_list_ibfk_1` FOREIGN KEY (`list_claim_id`) REFERENCES `claim` (`claim_id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;


CREATE TABLE `claim_tag`
(
    `id`          bigint unsigned                                              NOT NULL AUTO_INCREMENT,
    `tag_id`      bigint unsigned                                                       DEFAULT NULL,
    `claim_id`    varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    `created_at`  datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at` datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `Idx_claim_tag` (`tag_id`, `claim_id`),
    KEY `Idx_OuptutCreated` (`created_at`),
    KEY `Idx_OutputModified` (`modified_at`),
    KEY `claim_tag_ibfk_1` (`claim_id`),
    CONSTRAINT `claim_tag_ibfk_1` FOREIGN KEY (`claim_id`) REFERENCES `claim` (`claim_id`) ON DELETE CASCADE,
    CONSTRAINT `claim_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;


CREATE TABLE `gorp_migrations`
(
    `id`         varchar(255) NOT NULL,
    `applied_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `input`
(
    `id`                    bigint unsigned                                            NOT NULL AUTO_INCREMENT,
    `transaction_id`        bigint unsigned                                            NOT NULL,
    `transaction_hash`      varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `input_address_id`      bigint unsigned                                                     DEFAULT NULL,
    `is_coinbase`           tinyint(1)                                                 NOT NULL DEFAULT '0',
    `coinbase`              varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci         DEFAULT NULL,
    `prevout_hash`          varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci          DEFAULT NULL,
    `prevout_n`             int unsigned                                                        DEFAULT NULL,
    `prevout_spend_updated` tinyint(1)                                                 NOT NULL DEFAULT '0',
    `sequence`              int unsigned                                               NOT NULL,
    `value`                 double(18, 8)                                                       DEFAULT NULL,
    `script_sig_asm`        text CHARACTER SET latin1 COLLATE latin1_general_ci,
    `script_sig_hex`        text CHARACTER SET latin1 COLLATE latin1_general_ci,
    `created`               datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified`              datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `vin`                   int unsigned                                                        DEFAULT NULL,
    `witness`               text COLLATE utf8mb4_unicode_ci,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `FK_InputAddress` (`input_address_id`),
    KEY `FK_InputTransaction` (`transaction_id`),
    KEY `Idx_InputValue` (`value`),
    KEY `Idx_PrevoutHash` (`prevout_hash`),
    KEY `Idx_InputCreated` (`created`),
    KEY `Idx_InputModified` (`modified`),
    KEY `Idx_InputTransactionHash` (`transaction_hash`),
    KEY `Idx_TxHashVin` (`transaction_hash`, `vin`),
    CONSTRAINT `input_ibfk_2` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `job_status`
(
    `job_name`      varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `last_sync`     datetime                                                   NOT NULL DEFAULT '0001-01-01 00:00:00',
    `is_success`    tinyint(1)                                                 NOT NULL DEFAULT '0',
    `error_message` text COLLATE utf8mb4_unicode_ci,
    `state`         json                                                                DEFAULT NULL,
    PRIMARY KEY (`job_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `output`
(
    `id`                  bigint unsigned                                            NOT NULL AUTO_INCREMENT,
    `transaction_id`      bigint unsigned                                            NOT NULL,
    `transaction_hash`    varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `value`               double(18, 8)                                                       DEFAULT NULL,
    `vout`                int unsigned                                               NOT NULL,
    `type`                varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci          DEFAULT NULL,
    `script_pub_key_asm`  text CHARACTER SET latin1 COLLATE latin1_general_ci,
    `script_pub_key_hex`  text CHARACTER SET latin1 COLLATE latin1_general_ci,
    `required_signatures` int unsigned                                                        DEFAULT NULL,
    `address_list`        text CHARACTER SET latin1 COLLATE latin1_general_ci,
    `is_spent`            tinyint(1)                                                 NOT NULL DEFAULT '0',
    `spent_by_input_id`   bigint unsigned                                                     DEFAULT NULL,
    `created_at`          datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`         datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `claim_id`            varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci        DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `UK_Output` (`transaction_hash`, `vout`),
    KEY `FK_OutputTransaction` (`transaction_id`),
    KEY `FK_OutputSpentByInput` (`spent_by_input_id`),
    KEY `Idx_OutputValue` (`value`),
    KEY `Idx_Oupoint` (`vout`, `transaction_hash`) COMMENT 'needed for references in this column order',
    KEY `Idx_ASM` (`script_pub_key_asm`(255)) COMMENT 'needed in cases where the ASM needs to be parsed with speed',
    KEY `Idx_OuptutCreated` (`created_at`),
    KEY `Idx_OutputModified` (`modified_at`),
    KEY `fk_claim` (`claim_id`),
    KEY `Idx_IsSpent` (`is_spent`),
    KEY `Idx_SpentOutput` (`transaction_hash`, `vout`, `is_spent`) COMMENT 'used for grabbing spent outputs with joins',
    KEY `Idx_ModifedSpentTxHash` (`modified_at`, `is_spent`, `transaction_hash`),
    CONSTRAINT `output_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE CASCADE,
    CONSTRAINT `Cnt_AddressesValidJson` CHECK (((`address_list` is null) or json_valid(`address_list`)))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `support`
(
    `id`                  bigint unsigned                                              NOT NULL AUTO_INCREMENT,
    `supported_claim_id`  varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    `support_amount`      double(18, 8)                                                NOT NULL DEFAULT '0.00000000',
    `bid_state`           varchar(20) COLLATE utf8mb4_unicode_ci                       NOT NULL DEFAULT 'Accepted',
    `transaction_hash_id` varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci            DEFAULT NULL,
    `vout`                int unsigned                                                 NOT NULL,
    `created_at`          datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`         datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `Idx_state` (`bid_state`),
    KEY `Idx_supportedclaimid` (`supported_claim_id`),
    KEY `Idx_transaction` (`transaction_hash_id`),
    KEY `Idx_vout` (`vout`),
    KEY `Idx_outpoint` (`transaction_hash_id`, `vout`),
    CONSTRAINT `fk_transaction` FOREIGN KEY (`transaction_hash_id`) REFERENCES `transaction` (`hash`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `tag`
(
    `id`          bigint unsigned                                               NOT NULL AUTO_INCREMENT,
    `tag`         varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    `created_at`  datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at` datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `Idx_tag` (`tag`),
    KEY `Idx_OuptutCreated` (`created_at`),
    KEY `Idx_OutputModified` (`modified_at`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;


CREATE TABLE `transaction`
(
    `id`               bigint unsigned                                            NOT NULL AUTO_INCREMENT,
    `block_hash_id`    varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci          DEFAULT NULL,
    `input_count`      int unsigned                                               NOT NULL,
    `output_count`     int unsigned                                               NOT NULL,
    `fee`              double(18, 8)                                              NOT NULL DEFAULT '0.00000000',
    `transaction_time` bigint unsigned                                                     DEFAULT NULL,
    `transaction_size` bigint unsigned                                            NOT NULL,
    `hash`             varchar(70) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
    `version`          int                                                        NOT NULL,
    `lock_time`        int unsigned                                               NOT NULL,
    `raw`              mediumtext COLLATE utf8mb4_unicode_ci,
    `created_at`       datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `modified_at`      datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `created_time`     datetime                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `value`            double(58, 8)                                              NOT NULL DEFAULT '0.00000000',
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `Idx_TransactionHash` (`hash`),
    KEY `Idx_TransactionTime` (`transaction_time`),
    KEY `Idx_TransactionCreatedTime` (`created_time`),
    KEY `Idx_TransactionCreated` (`created_at`),
    KEY `Idx_TransactionModified` (`modified_at`),
    KEY `transaction_ibfk_1` (`block_hash_id`),
    KEY `value` (`value`),
    CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`block_hash_id`) REFERENCES `block` (`hash`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;


CREATE TABLE `transaction_address`
(
    `transaction_id` bigint unsigned NOT NULL,
    `address_id`     bigint unsigned NOT NULL,
    `debit_amount`   double(18, 8)   NOT NULL DEFAULT '0.00000000' COMMENT 'Sum of the inputs to this address for the tx',
    `credit_amount`  double(18, 8)   NOT NULL DEFAULT '0.00000000' COMMENT 'Sum of the outputs to this address for the tx',
    PRIMARY KEY (`transaction_id`, `address_id`),
    KEY `Idx_TransactionsAddressesAddress` (`address_id`),
    KEY `Idx_TransactionsAddressesDebit` (`debit_amount`),
    KEY `Idx_TransactionsAddressesCredit` (`credit_amount`),
    CONSTRAINT `transaction_address_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE CASCADE,
    CONSTRAINT `transaction_address_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = COMPRESSED
  KEY_BLOCK_SIZE = 4;

CREATE TRIGGER `tg_insert_value`
    AFTER INSERT
    ON `transaction_address`
    FOR EACH ROW
    UPDATE transaction
    SET transaction.value = transaction.value + NEW.credit_amount
    WHERE transaction.id = NEW.transaction_id;

CREATE TRIGGER `tg_insert_balance`
    AFTER INSERT
    ON `transaction_address`
    FOR EACH ROW
    UPDATE address
    SET address.balance = address.balance + (NEW.credit_amount - NEW.debit_amount)
    WHERE address.id = NEW.address_id;

CREATE TRIGGER `tg_update_value`
    AFTER UPDATE
    ON `transaction_address`
    FOR EACH ROW
    UPDATE transaction
    SET transaction.value = transaction.value - OLD.credit_amount + NEW.credit_amount
    WHERE transaction.id = NEW.transaction_id;

CREATE TRIGGER `tg_update_balance`
    AFTER UPDATE
    ON `transaction_address`
    FOR EACH ROW
    UPDATE address
    SET address.balance = address.balance - (OLD.credit_amount - OLD.debit_amount) +
                          (NEW.credit_amount - NEW.debit_amount)
    WHERE address.id = NEW.address_id;

CREATE TRIGGER `tg_delete_balance`
    AFTER DELETE
    ON `transaction_address`
    FOR EACH ROW
    UPDATE address
    SET address.balance = address.balance - (OLD.credit_amount - OLD.debit_amount)
    WHERE address.id = OLD.address_id;