# LBRY Decentralized DB setup
## Docker
```
docker-compose up -d
```

## Manual local setup

## Lbrycrd
Start wallet daemon downloading full blockchain data
```
./lbrycrdd -server -txindex -rpcuser=lbry -rpcpassword=lbry -conf=lbrycrd.conf
```

## Chainquery
Pulls block data from lbrycrdd and store it in a formatted format 
```
./chainquery --configpath config/ serve
```


## Current issues
For some reason constraint in `block` table fails and needs to be removed. 
```
ALTER TABLE block DROP CONSTRAINT Cnt_TransactionHashesValidJson;
```